#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#define MAP_SIZE                (0x00020)
#define MAP_BASE_ADDR           (0xFF200000) //lwAxiMaster Base Addr
#define FRAME_REG               ((0x0)/sizeof(unsigned int))
#define CAMERA_DATA_OFFSET      ((0x4)/sizeof(unsigned int))
#define IMAGESIZE_OFFSET        ((0x10)/sizeof(unsigned int))
#define CAMERA_INTERFACE_REG    ((0x14)/sizeof(unsigned int))



int main(int argc, char** argv)
{

    int fd;
    FILE *  image_file;
    int i;
    unsigned int *mapped_ptr;

    printf("\n\n Single Frame Demo \n\n");

    fd= open("/dev/mem", O_RDWR|O_SYNC);
    if(fd < 0){
        printf(" failed to open /dev/mem\n");
        return -1;
    }

    mapped_ptr = (unsigned int *)mmap(NULL,
            MAP_SIZE,
            PROT_READ|PROT_WRITE,
            MAP_SHARED,
            fd,
            MAP_BASE_ADDR );

    if(mapped_ptr == MAP_FAILED){
        printf("mmap failed\n");
        return -1;
    }

    int imagesize = mapped_ptr[(IMAGESIZE_OFFSET)];
    int width = (imagesize & 0xFFFF0000) >> 16;
    int height = imagesize & 0xFFFF;

    image_file= fopen("frame.pgm", "w+");
    if(fd < 0){
        printf(" failed to open frame.pgm\n");
        return -1;
    }

    fprintf(image_file, "P2 640 480 255\n");

    mapped_ptr[FRAME_REG] |= 0x4; //ReSync
    mapped_ptr[CAMERA_INTERFACE_REG] = 0x1;

	int count = 0;
       
    for(i=0; i < width*height/4; i++) {

        int b =  mapped_ptr[CAMERA_DATA_OFFSET];
        
        if(mapped_ptr[FRAME_REG] & 0x2) { //READ Error
            i--;
//		printf("Read Error");
        }else{
		count++;
            int b0 = (b & 0xFF) >> 0;
            int b1 = (b & 0xFF00) >> 8;
            int b2 = (b & 0xFF0000) >> 16;
            int b3 = (b & 0xFF000000) >> 24;

            fprintf(image_file, "%d %d %d %d ", b0, b1, b2, b3);
        }

    }

    if(mapped_ptr[FRAME_REG] & 0x1) { //WRITE Error
	printf("Write Error");
        mapped_ptr[FRAME_REG] |= 0x4; //ReSync
    }

    printf("Count: %d\n",count);

	printf("Agora foi");

    mapped_ptr[CAMERA_DATA_OFFSET] = 0x0;

    munmap(mapped_ptr, MAP_SIZE);

    fclose(image_file);

    close(fd);

    printf("\n done\n\n");

    return 0;
}

